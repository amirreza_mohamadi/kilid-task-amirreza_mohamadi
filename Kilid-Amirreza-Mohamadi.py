from dataclasses import dataclass
from fileinput import close
import pandas as pd
import re as re
from sqlalchemy import create_engine
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from mpl_finance import candlestick_ohlc
import matplotlib.dates as mpl_dates
import psycopg2



class Database:   
    host = "localhost"
    port = "5432"
    user = "postgres"
    passwd = "1"
    db = "usd_rl_price"

    def __init__(self):
        self
    
    def create_database(self):
        self.conn = psycopg2.connect(
           database="postgres", user=self.user, password=self.passwd, host=self.host, port= self.port
        )
        self.conn.autocommit = True
        cursor = self.conn.cursor()
        sql = '''CREATE database '''+self.db;
        #Creating a database
        try:
            cursor.execute(sql)
            print("Database created successfully........")
        except:
            pass
        self.conn.close()

    def connect_database(self):
        self.engine = create_engine('postgresql://'+self.user+':'+self.passwd+'@'+self.host+':'+self.port+'/'+self.db+'')
        self.dbConnection = self.engine.connect();

    def create_and_insert_to_table(self,table_name):
        self.connect_database()
        self.df.to_sql(table_name, self.engine, if_exists='replace', index_label='ID')

    def load_from_db(self,query):
        self.dataFrame = pd.read_sql(query, self.dbConnection);
        # return dataFrame
    
    def plot(self,type_of_chart):
        if type_of_chart == "close_price":
            close_dataframe = self.dataFrame.loc[:, ['Miladi_date','close']]
            close_dataframe = close_dataframe.sort_values(by="Miladi_date",ascending=True)
            close_dataframe['Miladi_date'] = close_dataframe['Miladi_date'].apply(mpl_dates.date2num)
            close_dataframe = close_dataframe.astype(float)
            close_dataframe = close_dataframe.set_index("Miladi_date")
            fig,axes=plt.subplots(1,1)
            axes.plot(close_dataframe.index, close_dataframe["close"])
            # axes.yaxis.set_major_locator(MaxNLocator(30)) 
            # axes.xaxis.set_major_locator(MaxNLocator(30)) 
            date_format = mpl_dates.DateFormatter('%d-%m-%Y')
            axes.xaxis.set_major_formatter(date_format)
            axes.set_xlabel('Date')
            axes.set_ylabel('Price(Rial)')
            fig.autofmt_xdate()
            plt.xticks(rotation=45)
            fig.tight_layout()
            fig.savefig('close_price.png') 
            plt.grid()
            plt.show()
        elif type_of_chart == "candlestick":
            plt.style.use('ggplot')
            ohlc = self.dataFrame.loc[:, ['Miladi_date', 'open', 'max', 'min', 'close']]
            ohlc['Miladi_date'] = ohlc['Miladi_date'].apply(mpl_dates.date2num)
            ohlc = ohlc.astype(float)
            fig, ax = plt.subplots()
            candlestick_ohlc(ax, ohlc.values, width=0.8, colorup='green', colordown='red', alpha=0.8)
            ax.set_xlabel('Date')
            ax.set_ylabel('Price')
            fig.suptitle('USD-RL Candlestick')
            date_format = mpl_dates.DateFormatter('%d-%m-%Y')
            ax.xaxis.set_major_formatter(date_format)
            fig.autofmt_xdate()
            fig.tight_layout()
            fig.savefig('candlestick.png') 
            plt.show()
        else:
            pass


    def crawl(self,link):
        self.df = pd.read_json(link)

    def preprocess(self):
        self.df = pd.DataFrame(self.df['data'].values.tolist())
        # df = df.drop(df[],inplace=True) 
        self.df = self.df.iloc[1: , :]
        # df['TimeStamp'] = pd.to_datetime(df['TimeStamp'])
        # df = df.set_index('TimeStamp')
        self.df = self.df.drop_duplicates()
        self.df = self.df.dropna()
        self.df[0] = self.df[0].apply(lambda cw : self.convert_float(cw))
        self.df[1] = self.df[1].apply(lambda cw : self.convert_float(cw))
        self.df[2] = self.df[2].apply(lambda cw : self.convert_float(cw))
        self.df[3] = self.df[3].apply(lambda cw : self.convert_float(cw))
        self.df[4]=self.df[4].apply(lambda cw : self.remove_tags(cw))
        self.df[5]=self.df[5].apply(lambda cw : self.remove_tags(cw))
        self.df.columns = ["open", "min", "max", "close","change","change_percentage","Miladi_date","Hijri_date"]
        self.df['Miladi_date'] = pd.to_datetime(self.df['Miladi_date'])

    def remove_tags(self,string):
        result = re.sub('<.*?>','',string)
        return result

    def convert_float(self,string):
        result = re.sub(',','',string)
        result = float(result)
        return result



if __name__ == "__main__":
    db = Database()
    link = "https://api.accessban.com/v1/market/indicator/summary-table-data/price_dollar_rl?lang=fa&order_dir=asc&draw=10&columns[0][data]=0&columns[0][name]=&columns[0][searchable]=true&columns[0][orderable]=true&columns[0][search][value]=&columns[0][search][regex]=false&columns[1][data]=1&columns[1][name]=&columns[1][searchable]=true&columns[1][orderable]=true&columns[1][search][value]=&columns[1][search][regex]=false&columns[2][data]=2&columns[2][name]=&columns[2][searchable]=true&columns[2][orderable]=true&columns[2][search][value]=&columns[2][search][regex]=false&columns[3][data]=3&columns[3][name]=&columns[3][searchable]=true&columns[3][orderable]=true&columns[3][search][value]=&columns[3][search][regex]=false&columns[4][data]=4&columns[4][name]=&columns[4][searchable]=true&columns[4][orderable]=true&columns[4][search][value]=&columns[4][search][regex]=false&columns[5][data]=5&columns[5][name]=&columns[5][searchable]=true&columns[5][orderable]=true&columns[5][search][value]=&columns[5][search][regex]=false&columns[6][data]=6&columns[6][name]=&columns[6][searchable]=true&columns[6][orderable]=true&columns[6][search][value]=&columns[6][search][regex]=false&columns[7][data]=7&columns[7][name]=&columns[7][searchable]=true&columns[7][orderable]=true&columns[7][search][value]=&columns[7][search][regex]=false&start=0&length=365&search=&order_col=&order_dir=&from=&to=&convert_to_ad=1&_=1655531628378"
    db.crawl(link)
    db.preprocess()
    db.create_database()
    db.create_and_insert_to_table('gheymat')
    db.load_from_db("select \"Miladi_date\", open,max,min,close  from \"gheymat\"")
    db.plot("close_price")
    db.plot("candlestick")
 
